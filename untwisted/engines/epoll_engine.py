import select
import time
import traceback
import logging
import errno

class EpollEngine():
	_log = logging.getLogger(__name__)
	_epoll_engine = None
	_fd_to_socket = {}

	def Start(self):
		self._epoll_engine = select.epoll()
	
	def Stop(self):
		self._epoll_engine.close()

	def RegisterSocket(self, socket):
		socket.events = 0
		self._epoll_engine.register(socket.GetFD(), 0)
		self._fd_to_socket[socket.GetFD()] = socket
	
	def UnregisterSocket(self, socket):
		self._epoll_engine.unregister(socket.GetFD())
		del self._fd_to_socket[socket.GetFD()]
	
	def RequestRead(self, socket):
		socket.events |= select.EPOLLIN
		self._epoll_engine.modify(socket.GetFD(), socket.events)
	
	def CancelRead(self, socket):
		socket.events &= ~select.EEPOLLIN
		self._epoll_engine.modify(socket.GetFD(), socket.events)
	
	def RequestWrite(self, socket):
		socket.events |= select.EPOLLOUT
		self._epoll_engine.modify(socket.GetFD(), socket.events)
	
	def CancelWrite(self, socket):
		socket.events &= ~select.EPOLLOUT
		self._epoll_engine.modify(socket.GetFD(), socket.events)
	
	def Poll(self):
		if len(self._fd_to_socket) == 0:
			time.sleep(1)
			return

		try:
			events = self._epoll_engine.poll(1)
		except IOError, ex:
			if ex.errno != errno.EINTR:
				self._log.error("Error in epoll.Poll: " + str(ex))
				traceback.print_exc()
				time.sleep(1)
			return
		for fd, event in events:
			socket = self._fd_to_socket[fd]
			if not socket:
				continue

			if event & (select.EPOLLERR | select.EPOLLHUP):
				socket.ProcessError()
				continue

			try:
				if event & select.EPOLLIN:
					socket.ProcessRead()
				if event & select.EPOLLOUT:
					socket.ProcessWrite()
			except Exception, ex:
				traceback.print_exc()
				socket.OnError(ex)
				socket.Shutdown()


