import thread
from pipe import Pipe

class ThreadPipe(Pipe):
	at_exit = None
	args = None

	def __init__(self, reactor, at_exit, args):
		Pipe.__init__(self, reactor)
		self.at_exit = at_exit
		self.args = args

	def OnRead(self, line):
		self.at_exit(*self.args)
		self.Shutdown()

def threadEntry(pipe, func, args):
	func(*args)
	if pipe:
		pipe.Write()

def deferToThread(func, reactor, at_exit, *args):
	pipe = None
	if reactor and at_exit:
		pipe = ThreadPipe(reactor, at_exit, args)
	thread.start_new_thread(threadEntry, (pipe, func, args))


