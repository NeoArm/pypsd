# module for pypseudoserver
# written by radicand
# doesn't do much by itself

from psmodule import *
from untwisted import task
import random
import hashlib

class PSModule_loadtester(PSModule):
	VERSION = 1
	uid = ""
	my_users = {}
	CONNS = 2500

	def startup(self):
		if not PSModule.startup(self): return False

		cfu = self.parent.createFakeUser
		for i in range(0,self.CONNS):
			nick = "py-%i" % i
			user = "wat"
			host = "test.%i" % i
			gecos = "adam%i" % i
			nspass = "fakemofo"
			modes = "Nyo"
			
			try:
				uid,user = cfu(
					nick,
					user,
					host,
					gecos,
					modes,
					nspass,
					version="PyPsd Loadtester v%d" % self.VERSION,
					)
				self.my_users[uid] = user
			except Exception, err:
				self.log.exception("Error creating load module user: %s" % err)

		self.task_havefun = task.LoopingCall(self.timer_havefun)
		self.task_havefun.start(1) # every 1 second
		return True
	
	def shutdown(self):
		PSModule.shutdown(self)
		try: self.task_havefun.stop()
		except: pass
		qfu = self.parent.quitFakeUser
		for uid, users in self.my_users.iteritems():
			qfu(uid)
			
	def timer_havefun(self):
		sm = self.parent.sendMessage
		m = hashlib.md5()
		t = self.parent.mkts()
		
		r = random.random
		
		for uid,users in self.my_users.iteritems():
			newnick = m.update(str(r()))
			newnick = m.hexdigest()[5:15]
			sm("NICK", "py-%s :%i" % (newnick, t), prefix=uid)
		
