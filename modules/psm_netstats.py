#!/usr/bin/python pseudoserver.py
#psm-netstats.py
# module for pypseudoserver
# written by radicand
# useful functions for network status reporting

from untwisted import threading
from untwisted import task
from untwisted.istring import istring
from psmodule import *
import re, datetime,sys
import networkx as nx

class PSModule_netstats(PSModule):
	VERSION = 5
	
	uid = ""

	splitserver = None
	
	def __init__(self, parent, config):
		PSModule.__init__(self, parent, config)
		self.log = logging.getLogger(__name__)
		
		try:
			self.routechans = istring(config.get('netstats-split', 'channel')).split(',')
		except Exception, err:
			self.log.exception("Error reading 'netstats-split:channel' configuration option: %s" % err)
			raise

		self.klinere = re.compile(r"(?:\[G\])?(?:\[(.+)\]\s)?(.*)")
		#below: ('hybrid', '7.2.3', '20081028', '520')
		# ^^ 0software, 1version, 2build date, 3build #
		self.versionre = re.compile(r"([^\d]+)-((?:\d+\.)*\d+)(?:.*\((\d+)_\d+-(\d+))?")
		
	def startup(self):
		if not PSModule.startup(self): return False

		try:
			self.dbp.execute("CREATE TABLE IF NOT EXISTS split (item INT(10) NOT NULL AUTO_INCREMENT, s1 VARCHAR(255), s2 VARCHAR(255), \
				ts TIMESTAMP, remarks VARCHAR(255), active INT(10), PRIMARY KEY (item));")
			self.dbp.execute("CREATE TABLE IF NOT EXISTS link (s1 VARCHAR(255), s2 VARCHAR(255), ts TIMESTAMP);")
			self.dbp.execute("CREATE TABLE IF NOT EXISTS kline (oper VARCHAR(255), user VARCHAR(255), ip VARCHAR(255), message VARCHAR(255), created TIMESTAMP,\
				expires TIMESTAMP);")
			self.dbp.execute("CREATE TABLE IF NOT EXISTS kills (target VARCHAR(255), targetsu VARCHAR(255),\
				oper VARCHAR(255), opersu VARCHAR(255), server VARCHAR(255), message TEXT, killed TIMESTAMP);")
			self.dbp.execute("CREATE TABLE IF NOT EXISTS state (sid VARCHAR(255), server VARCHAR(255), users INT(10), opers INT(10), uptime INT(10),\
				seen TIMESTAMP, graphed INT(10), ircd VARCHAR(255), version VARCHAR(255), build INT(10), first_seen TIMESTAMP,\
				UNIQUE KEY (sid), UNIQUE KEY (server));")
			self.dbp.execute("CREATE TABLE IF NOT EXISTS sustate (uid VARCHAR(255), nick VARCHAR(255), su VARCHAR(255), flags VARCHAR(255), version VARCHAR(255));")
			self.dbp.execute("CREATE TABLE IF NOT EXISTS chanstate (channel VARCHAR(255), users INT(10), topic VARCHAR(255), modes VARCHAR(255));")
			self.dbp.execute("CREATE TABLE IF NOT EXISTS statso (sid VARCHAR(255), mask VARCHAR(255), name VARCHAR(255), modes VARCHAR(255));")
		except Exception, err:
			self.log.exception("Error creating tables and indexes for netstats module: %s" % err)
			raise

		try:
			self.uid, user = self.parent.createFakeUser(
				self.config.get('netstats-split', 'nick'),
				self.config.get('netstats-split', 'user'),
				self.config.get('netstats-split', 'host'),
				self.config.get('netstats-split', 'gecos'),
				self.config.get('netstats-split', 'modes'),
				self.config.get('netstats-split', 'nspass'),
				join_chans = self.routechans
			)
		except Exception, err:
			self.log.exception("Error creating user for netstats module, is the config file correct? (%s)" % err)
			raise
		
		#network state routine
		self.task_netstate = task.LoopingCall(self.timer_saveNetState)
		self.task_netstate.start(120) # run every 2 minutes

		self.restoreSU()
		
		return True
	
	def shutdown(self):
		PSModule.shutdown(self)
		try:
			self.task_netstate.stop() #stop timer
		except Exception, err:
			self.log.exception("Error shutting down netstats savestate task (%s)" % err)

		#don't lose all that SU data we logged
		self.saveSU()
		
		self.parent.quitFakeUser(self.uid)
	
	def getVersion(self):
		return self.VERSION
		
	def countServers(self):
		"""Returns the number of online servers including itself"""
		
		return len(self.parent.server_t)
			
	def countUsers(self, options=0):
		"""Returns the number of online users, including its own
		If called with "options=1" returns avg users per server
		"""
		
		users = len(self.parent.user_t)
		if options == 0: return users #count
		elif options == 1: return users/self.countServers() #avg
			
	def countChannels(self, options=0):
		"""Returns the number of channels in existence."""
		
		chans = len(self.parent.channel_t)
		if options == 0: return chans
		elif options == 1: return chans # unimpl

	def drawGraph(self, target):
		"""Create network graph representation in XML, save to file"""
		
		xml = '<?xml version="1.0"?><graph title="Network Map" bgcolor="ffffff" linecolor="cccccc" viewmode="display" width="1000" height="750">\n'
		for sid,svr in self.parent.server_t.iteritems():
			if sid.endswith("C"):
				color = "0000ff"
				scale = "45"
				type = "CircleNode"
			else:
				color = "ff0000"
				scale = "50"
				type = "CircleTextNode"
			xml += '<node id="%s" text="%s" scale="%s" color="%s" textcolor="0000ff" type="%s"/>\n' % (sid, svr['host'], scale, color, type)
		for sid,svr in self.parent.server_t.iteritems():
			if svr['uplink_sid']: #check if exists, fix graph bug
				xml += '<edge label="" sourceNode="%s" targetNode="%s" textcolor="555555"/>\n' % (svr['uplink_sid'], sid)
				
		xml += '</graph>'
		
		file = open('graph.xml', 'w')
		file.write(xml.encode('utf-8'))
		file.close()
			
		return True
	
	def drawGraph_cb(self, target):
		"""Callback method for graph drawer
		Let the requestor know where it was saved
		"""
		
		self.parent.privMsg(target, "Done drawing graph.")
	
	def saveSU(self):
		"""Save service-user data to a database while we (probably) restart"""

		self.dbp.execute("BEGIN")
		self.dbp.execute("DELETE FROM sustate")
		for uid, user in self.parent.user_t.iteritems():
			if not user['su']: continue
			try:
				self.dbp.execute(u"INSERT INTO sustate VALUES (%s,%s,%s,%s,%s)", (uid, user['su'], user['nick'], user['flags'], user['version']))
			except Exception, err:
				self.log.exception("Error saving SUstate for %s:%s:%s:%s:%s (%s)" % (uid, user['su'], user['nick'], user['flags'], user['version'], err))
		self.dbp.execute("COMMIT")

	def restoreSU(self):
		"""Restore service-user data after a restart"""

		user_t = self.parent.user_t
		self.dbp.execute("SELECT * FROM sustate")

		for row in self.dbp.fetchall():
			#speed hack required
			if row[0] in user_t:
				user_t[row[0]]['su'] = row[1]
				user_t[row[0]]['flags'] = row[3]
				user_t[row[0]]['version'] = row[4]

	def timer_saveNetState(self):
		"""Periodically runs and saves network state to the database
		State table = server_name | users | opers | uptime(?)
		"""
		total = [0,0]
		server_t = self.parent.server_t
		user_t = self.parent.user_t
		channel_t = self.parent.channel_t
		self.dbp.execute("BEGIN")
		self.dbp.execute("DELETE FROM statso") #may be a better way to do this
		now = self.parent.mkts()
		for sid,svr in server_t.iteritems():
			#self.parent.sendMessage("STATS", "u|%s" % svr[1], prefix=self.uid)
			#self.parent.sendMessage("STATS", "o %s" % sid, prefix=self.uid)
			users = 0
			opers = 0
			for uid,usr in user_t.iteritems():
				if usr['sid'] == sid:
					if self.parent.has_umode(uid,'o'):
						opers += 1
					else:
						users += 1
			try:
				total[0] += users
				total[1] += opers
				self.log.debug("Saving %d users, %d opers for %s" % (users,opers,svr['host']))
				self.dbp.execute("INSERT INTO state (sid,server,users,opers,seen,graphed,first_seen) VALUES (%s,%s,%s,%s,FROM_UNIXTIME(%s),'0',NOW()) \
					ON DUPLICATE KEY UPDATE sid=%s, server=%s, users=%s, opers=%s, seen=FROM_UNIXTIME(%s), graphed='0';",
					(str(sid),str(svr['host']),str(users),str(opers),str(now),str(sid),str(svr['host']),str(users),str(opers),str(now)))

			except Exception, err:
				self.log.exception("Error saving server state for netstats: %s" % err)
				continue
		try:
			self.dbp.execute("INSERT INTO state (sid,server,users,opers,seen,graphed) VALUES (%s,%s,%s,%s,FROM_UNIXTIME(%s),'0') \
				ON DUPLICATE KEY UPDATE sid=%s, server=%s, users=%s, opers=%s, seen=FROM_UNIXTIME(%s), graphed=0;",
				("000","TOTAL",total[0],total[1],now,"000","TOTAL",total[0],total[1],now))
		except Exception, err:
			self.log.exception("Error saving TOTAL server state for netstats: %s" % err)
		try:
			self.dbp.execute("UPDATE state SET opers=0, users=0 WHERE seen < DATE_SUB(NOW(), INTERVAL 5 MINUTE) AND (opers > 0 OR users > 0);")
			pass
		except Exception, err:
			self.log.exception("Error resetting oper/user count to 0 for split servers: %s" % err)
		self.dbp.execute("COMMIT")
		
		# log total channel data
		self.dbp.execute("BEGIN")
		try:
			#flush old records
			self.dbp.execute("DELETE FROM chanstate")
			
			#log top 20? public channels
			# unimplemented
			
			#log total channels
			self.dbp.execute("INSERT INTO chanstate VALUES (%s,%s,%s,%s)",
				("TOTAL", len(channel_t), "", ""))
		except Exception, err:
			self.log.exception("Error logging channel state to database: %s" % err)
		self.dbp.execute("COMMIT")
			


## Begin event hooks
	def netstats_213(self, prefix, params):
		if self.splitserver:
			if params[4] == self.splitserver:
				self.parent.privMsg(self.routechans, "Server %s can connect to %s:%s" %
					(self.parent.get_server(prefix)[1]['host'], params[4], params[5]), self.uid)
		else:
			self.parent.privMsg(self.routechans, "Server %s can connect to %s:%s" %
				(self.parent.get_server(prefix)[1]['host'], params[4], params[5]), self.uid)
			
	def netstats_242(self, prefix, params):
		"""Handle server STATS u (uptime) responses"""
		
		grp = re.match('Server Up (\d+) days, (\d+):(\d+):(\d+)', params[0])
		if grp:
			#insert into db
			pass
	
	def netstats_243(self, prefix, params):
		"""Handles server STATS o (oper blocks) responses"""

		sid,svr = self.parent.get_server(prefix)

		try:
			self.dbp.execute(u"INSERT INTO statso VALUES (%s,%s,%s,%s)",
				(sid, params[2], params[4], params[5]))
		except Exception, err:
			self.log.exception("Error updating statso: %s" % (err,))
	
	def netstats_351(self, prefix, params):
		"""Handle server version replies
		[351](remote_sid): my_sid|hybrid-7.2.3+plexus-3.0.1(20081028_0-520).|irc.plzhalp.us|eGgIKMZ6 TS6ow
		"""
		
		sid, server = self.parent.get_server(prefix)
		if not server:
			self.log.warning("SECURITY: received forged? version response from %s: %s" %
				(prefix, "|".join(params)))
			return
		
		try:
			ver = self.versionre.match(params[1]).groups()
		except Exception, err:
			self.log.exception("Error, unable to parse version reply: %s (%s)" %
				(params[1], err))
			return
		
		self.log.debug("version reply from %s: %s v%s" % (params[2], ver[0], ver[3]))
		
		try:
			self.dbp.execute(u"UPDATE state SET ircd=%s, version=%s, build=%s WHERE sid=%s",
				(ver[0], ver[1], ver[3], prefix))
		except Exception, err:
			self.log.exception("Error updating state table with version data: %s" % (err,))

	def netstats_SQUIT(self, prefix, params):
		"""Handle server quits, log to splits table and alert channels"""
		
		try:
			psid, pserver = self.parent.get_server(
				nx.algorithms.shortest_paths.generic.shortest_path(
					self.parent.server_tree,
					self.parent.sid, params[0])[-2])
		except Exception, err:
			self.log.exception("Error using nx path traversal for SQUIT: %s" % err)
			return

		ssid, sserver = self.parent.get_server(params[0])
		try:
			self.dbp.execute("INSERT INTO split (s1,s2,ts,remarks,active) VALUES (%s, %s, NOW(), %s, 1);",
				(pserver['host'], sserver['host'], params[1]))
		except Exception, err:
			self.log.exception("Error updating split table for netstats module: %s" % err)

		self.parent.privMsg(self.routechans, "Server split detected: #%d: %s <-/-> %s: %s" %
			(self.dbp.lastrowid, pserver['host'], sserver['host'], params[1]))

		#trigger a statsc check on this server
		self.cmd_statsc(self.logchan, None, (sserver['host'],))

		
	def netstats_SID(self, prefix, params):
		"""Handle server introductions, log and alert channels"""
		
		lsid, lserver = self.parent.get_server(prefix)
		self.parent.privMsg(self.routechans, "Server link detected: %s <-> %s" %
			(lserver['host'], params[0]))
		try:
			self.dbp.execute("INSERT INTO link VALUES (%s, %s, NOW());",
				(lserver['host'], params[0]))
		except Exception, err:
			self.log.exception("Error updating link table for netstats module: %s" % err)

	def netstats_KILL(self, prefix, params):
		"""Handle kills, log to database"""
		
		#TODO perhaps move the exempts to config
		if (not self.parent.is_id(prefix) or
			len(prefix) == 3 or
			prefix[:3] == "02S" or
			prefix[:3] == "97H"): return #not ts6, not a user, and not svcs
		message = " ".join(params[1].split(" ")[1:])[1:-1]
		
		kuid, kuser = self.parent.get_user(params[0])
		ouid, ouser = self.parent.get_user(prefix)
		sid, server = self.parent.get_server(prefix[:3])
		
		try:
			self.dbp.execute("INSERT INTO kills VALUES (%s,%s,%s,%s,%s,%s,NOW());",
				(
				kuser['nick'],
				kuser['su'] or kuser['nick'],
				ouser['nick'],
				ouser['su'] or ouser['nick'],
				server['host'],
				message))
		except Exception, err:
			self.log.exception("Error updating kills table for netstats module: %s" % err)
	
	def netstats_KLINE(self, prefix, params):
		"""Handle klines, log to database"""
		
		uid, user = self.parent.get_user(prefix)
		rex = self.klinere.match(params[4])
		try:
			if rex.group(1):
				setter = "%s/%s" % (rex.group(1), user['nick'])
			else: setter = user['nick']
		except Exception, err:
			self.log.exception("Error, badly formed KLINE string parsed by netstats module: %s" % err)
		
		try:	
			self.dbp.execute(u"INSERT INTO kline VALUES(%s,%s,%s,%s,NOW(),DATE_ADD(NOW(), INTERVAL %s SECOND));",
				(setter, params[2], params[3], rex.group(2), params[1]))
		except Exception, err:
			self.log.exception("Error updating kline table for netstats module: %s" % err)

	def netstats_UNKLINE(self, prefix, params):
		"""Handle unklines, remove from database"""
		
		try:	
			self.dbp.execute("DELETE FROM kline WHERE ip=%s",
				(params[2],))
		except Exception, err:
			self.log.exception("Error updating (un)kline table for netstats module: %s" % err)
## End event hooks

### Begin command handlers
	def cmd_statsc(self, source, target, pieces):
		"""Runs STATS c against all HUBS"""

		if pieces:
			self.splitserver = pieces[0]
		else:
			self.splitserver = None

		for sid,svr in self.parent.server_t.iteritems():
			if sid.endswith("H") or sid.endswith("Z"):
				self.parent.sendMessage("STATS", "c %s" % sid, prefix=self.parent.uid)
		return True

	def cmd_restoresu(self, source, target, pieces):
		"""Restore SU data after a restart"""
		try:
			self.restoreSU()
			self.parent.privMsg(target, "SU State restored")
			return True
		except Exception, err:
			self.log.exception("Error in restoresu: %s" % err)
			return False
		

	def cmd_netstats(self, source, target, pieces):
		"""Return basic network statistics"""
		
		self.parent.privMsg(target,
			"%d servers are online, holding %d users (%d avg) on %d channels." %
			(self.countServers(), self.countUsers(),
			self.countUsers(1), self.countChannels()))

		return True
		
	def cmd_servertrace(self, source, target, pieces):
		"""Return a server tracepath from one server to another
		default from server is me
		"""
		
		if not pieces: return False
		try:
			if len(pieces) > 1:
				ssid, sserver = self.parent.get_server(pieces[0])
				dsid, dserver = self.parent.get_server(pieces[1])
				if not sserver:
					self.parent.privMsg(target,
						"Invalid source name")
					return True
			else:
				ssid, sserver = self.parent.get_server(self.parent.sid)
				dsid, dserver = self.parent.get_server(pieces[0])
			trace = nx.algorithms.shortest_paths.generic.shortest_path(
				self.parent.server_tree, ssid, dsid)
	
			trace = [v['host'] for k,v in map(self.parent.get_server,trace)]
			self.parent.privMsg(target,
				"STRACE: %s" % " -> ".join(trace))
		except Exception, err:
			self.parent.privMsg(target, "STRACE: Path not possible")
			self.log.exception("Error calculating servertrace in netstats module: %s" % err)
			
		return True
		
	def cmd_drawgraph(self, source, target, pieces):
		"""Call relevant methods to draw the network graph"""
		
		threading.deferToThread(self.drawGraph, self.parent.socket._reactor, self.drawGraph_cb, target)
		return True
		
	def cmd_splitnote(self, source, target, pieces):
		"""Add or update notes for server splits in the database"""
		
		if not pieces or len(pieces) < 2: return False

		uid, user = self.parent.get_user(source)

		try:
			self.dbp.execute(u"UPDATE split SET remarks=%s WHERE item=%s;",
				(u"%s: %s" %
					(user['nick'], " ".join(pieces[1:])),
				pieces[0]))
			self.parent.privMsg(target,
				"Splitnote for %s updated by %s." %
				(pieces[0], user['nick']), self.uid)
		except Exception, err:
			self.parent.privMsg(target, "Bad split number", self.uid)
			self.log.exception("Error updating splitremarks for netstats module: %s" % err)
			
		return True
		
	def cmd_splitlast(self, source, target, pieces):
		"""Returns the last few splits and the information about them"""
		
		try:
			try: limiter = min(15,int(pieces[0]))
			except (ValueError, IndexError): limiter = 5
			self.dbp.execute("SELECT item, s1, s2, ts, remarks FROM split WHERE active=1 ORDER BY ts DESC LIMIT %s;",
				(limiter,))
			self.parent.privMsg(target,
				"Displaying last %d splits..." % limiter, self.uid)
			for row in self.dbp.fetchall():
				self.parent.privMsg(target,
					"#%s: %s <-/-> %s @ %s: %s" %
					(row[0],row[1],row[2],
					row[3],
					row[4]), self.uid)
		except Exception, err:
			self.parent.privMsg(target, "Bad arguments.  Syntax: <# splits to return (max15)>", self.uid)
			
		return True
		
	def cmd_splitget(self, source, target, pieces):
		"""Returns a specific split and its information"""
		
		if not pieces: return False
		try:
			rowid = int(pieces[0])
		except (ValueError, IndexError):
			return False

		try:
			num = self.dbp.execute("SELECT s1, s2, ts, remarks FROM split WHERE item=%s;",
				(rowid,))
			if not num:
				self.parent.privMsg(target,
					"No such split ID", self.uid)
			else:
				row = self.dbp.fetchone()
				self.parent.privMsg(target,
					"#%s: %s <-/-> %s @ %s: %s" %
					(rowid,row[0],row[1],
					row[2],
					row[3]), self.uid)
		except Exception, err:
			self.log.exception("Error, unknown error in splitget of netstats module: %s" % err)
			
		return True
		
	def cmd_splitrank(self, source, target, pieces):
		"""Returns a list of all active splits grouped and sorted by server/amount"""
		
		try:
			self.dbp.execute("SELECT COUNT(*), s2 FROM split WHERE active=1 GROUP BY s2 ORDER BY COUNT(*) DESC;")
			for row in self.dbp.fetchall():
				self.parent.notice(source,
					"%sx: %s" % (row[0], row[1]), self.uid)
		except Exception, err:
			self.parent.privMsg(target, "Error fetching rankings.", self.uid)
			self.log.exception("Error fetching splitrank for netstats module: %s" % err)
			
		return True
		
	def cmd_splitreset(self, source, target, pieces):
		"""Marks all splits for server as unactive
		(not counted in talleys, but not deleted)
		"""
		
		if not pieces: return False
		try:
			self.dbp.execute("UPDATE split SET active=0 WHERE s2=%s", (pieces[0],))
			self.parent.privMsg(target,
				"%s deactivated in splits talley" % pieces[0], self.uid)
		except Exception, err:
			self.parent.privMsg(target,
				"Error resetting count.  Bad server name?", self.uid)
			self.log.exception("Error in splitreset of netstats module: %s" % err)

		return True

	def cmd_sdel(self, source, target, pieces):
		"""Deletes a server from the netstats database
		TODO: tell user when a server isnt in the db
		"""
		
		if not pieces: return False
		try:
			num = self.dbp.execute("DELETE FROM state WHERE server=%s", (pieces[0],))
			if num:
				self.parent.privMsg(target,
					"%s deleted from netstats system." % pieces[0], self.uid)
			else:
				self.parent.privMsg(target,
					"%s does not exist within the netstats system." % pieces[0], self.uid)

		except Exception, err:
			self.parent.privMsg(target,
				"Error deleting server.  Bad server name?", self.uid)
			self.log.exception("Error in sdel of netstats module: %s" % err)

		return True
	
	def cmd_smerge(self, source, target, pieces):
		"""Merges all relevent data from oldserver into newserver"""

		if len(pieces) < 2: return False

		#check existence in sql
		self.dbp.execute("SELECT COUNT(*) FROM state WHERE server = %s OR server = %s", (pieces[0], pieces[1]))
		found = self.dbp.fetchone()[0]

		#check rrd existence
		import os
		try:
			rpath = self.config.get('netstats', 'rrd_dir')
			c1 = os.path.isfile("%s/%s.rrd" % (rpath, pieces[0]))
			c2 = os.path.isfile("%s/%s.rrd" % (rpath, pieces[1]))
		except:
			c1 = False
			c2 = False

		#check for errors
		if not (found == 2 or (c1 and c2)):
			self.parent.privMsg(target, "Unable to merge, %d targets do not exist in database or RRDs" % (2-found,))
			return True

		#update db
		if found == 2:
			self.dbp.execute("UPDATE kills SET server=%s WHERE server=%s", (pieces[1], pieces[0]))
			self.dbp.execute("UPDATE link SET s1=%s WHERE s1=%s" (pieces[1], pieces[0]))
			self.dbp.execute("UPDATE link SET s2=%s WHERE s2=%s", (pieces[1], pieces[0]))
			self.dbp.execute("UPDATE split SET s1=%s WHERE s1=%s", (pieces[1], pieces[0]))
			self.dbp.execute("UPDATE split SET s2=%s WHERE s2=%s", (pieces[1], pieces[0]))
			self.dbp.execute("DELETE FROM state WHERE server=%s", (pieces[0],))
	
			self.parent.privMsg(target, "Updated %s => %s in DB" % (pieces[0], pieces[1]))

		#update rrds
		# YOU MUST HAVE THE MERGE TOOL
		tpath = os.getcwd()+ "/modules/merged-rrd.py"
		if not os.path.isfile(tpath) or not (c1 and c2):
			self.parent.privMsg(target, "Not updating RRDs, merge tool does not exist or RRDs dont exist")
			return True

		r1 = "%s/%s.rrd" % (rpath, pieces[0])
		r2 = "%s/%s.rrd" % (rpath, pieces[1]) 
		r3 = "%s/tmp_merge.rrd" % (rpath,)
		
		import shutil
		shutil.copy2(r1, r1 + ".bak")
		shutil.copy2(r2, r2 + ".bak")
		os.system("%s %s %s %s" % (tpath, r1, r2, r3))
		os.rename(r3, r2)

		self.parent.privMsg(target, "Updated %s => %s in RRDs" % (pieces[0], pieces[1]))

		return True
### End command handlers

	def getCommands(self):
		return (
			('sreset', {
			'permission':'s',
			'callback':self.cmd_splitreset,
			'usage':"<server name> - resets a server's split count for the talley"}),
			('srank', {
			'permission':'s',
			'callback':self.cmd_splitrank,
			'usage':"- shows how many times all servers have split"}),
			('sget', {
			'permission':'s',
			'callback':self.cmd_splitget,
			'usage':"<split id> - get information related to the split id specified"}),
			('slast', {
			'permission':'s',
			'callback':self.cmd_splitlast,
			'usage':"[number of entries, 15 max|default=5] - returns the last X number of splits"}),
			('snote', {
			'permission':'s',
			'callback':self.cmd_splitnote,
			'usage':"<split id> <remarks> - change the split remarks on split id to remarks"}),
			('drawgraph', {
			'permission':'s',
			'callback':self.cmd_drawgraph,
			'usage':"- draws the current network graph and tells you where it's saved"}),
			('strace', {
			'permission':'s',
			'callback':self.cmd_servertrace,
			'usage':"[source server name|default=me] <destination server name> - trace the link path from source to destination"}),
			('netstats', {
			'permission':'s',
			'callback':self.cmd_netstats,
			'usage':"- shows current number of linked servers, online users, and average per server"}),
			('restoresu', {
			'permission':'s',
			'callback':self.cmd_restoresu,
			'usage':"- restores SU data after a restart"}),
			('smerge', {
			'permission':'s',
			'callback':self.cmd_smerge,
			'usage':"<old server> <new server> - merges old server records (kills, rrds, state, etc) to new server"}),
			('statsc', {
			'permission':'s',
			'callback':self.cmd_statsc,
			'usage':"[end leaf server] - performs a 'STATS c' for a server"}),
			('sdel', {
			'permission':'s',
			'callback':self.cmd_sdel,
			'usage':"[server] - deletes a server from the netstats website (does not remove graphs on disk)"}),
		)

	def getHooks(self):
		return (
			('squit', self.netstats_SQUIT),
			('sid', self.netstats_SID),
			('kill', self.netstats_KILL),
			('kline', self.netstats_KLINE),
			('unkline', self.netstats_UNKLINE),
			('351', self.netstats_351),
			('243', self.netstats_243),
			('213', self.netstats_213),
		)
