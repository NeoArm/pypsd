from collection import *
from sys_base import *

class User(CollectionEntity):
  def __init__(self, name, flags, ban_source = None, ban_reason = None, ban_date = None, ban_expiry = None, citizen = None, company = None):
    CollectionEntity.__init__(self, name, flags, ban_source, ban_reason, ban_date, ban_expiry)
    self.citizen = citizen
    self.company = company

  def clear(self):
    self.citizen = None
    self.company = None

class UserManager(CollectionManager, Subsystem):
  def __init__(self, module):
    Subsystem.__init__(self, module, module.options, 'users')
    CollectionManager.__init__(self, User)
    self.db_open()
    self.cursor.execute("CREATE TABLE IF NOT EXISTS esim_users (id INT NOT NULL AUTO_INCREMENT, name VARCHAR(51) NOT NULL, flags BIGINT NOT NULL, ban_source TEXT, ban_reason TEXT, ban_date INT, ban_expiry INT, citizen INT, company INT, PRIMARY KEY (id), UNIQUE KEY (name))")
    self.cursor.execute("SELECT name, flags, ban_source, ban_reason, ban_date, ban_expiry, citizen, company FROM esim_users")

    for row in self.cursor.fetchall():
      name = istring(row[0])
      flags = int(row[1])
      ban_source = istring(row[2]) if row[2] != None else None
      ban_reason = istring(row[3]) if row[3] != None else None
      ban_date = int(row[4]) if row[4] != None else None
      ban_expiry = int(row[5]) if row[5] != None else None
      citizen = int(row[6]) if row[6] != None else None
      company = int(row[7]) if row[7] != None else None
      self[name] = User(name, flags, ban_source, ban_reason, ban_date, ban_expiry, citizen, company)

  def commit(self):
    try:
      deleted_users = [(str(user), ) for user in self.list_deleted()]
      changed_users = [(str(user.name), user.flags, user.ban_source, user.ban_reason, user.ban_date, user.ban_expiry, user.citizen, user.company) for user in self.list_dirty()]

      if len(deleted_users) > 0:
        self.cursor.executemany("DELETE FROM esim_users WHERE name = %s", deleted_users)
        self.module.elog.commit('Deleted %d users from database.' % len(deleted_users))

      if len(changed_users) > 0:
        self.cursor.executemany("REPLACE INTO esim_users (name, flags, ban_source, ban_reason, ban_date, ban_expiry, citizen, company) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", changed_users)
        self.module.elog.commit('Committed %d users to database.' % len(changed_users))

      self.clear_deleted()

      for user in self.list_all():
        user.dirty = False
    except Exception, err:
      self.module.elog.error('User commit failed: @b%s@b' % err)
