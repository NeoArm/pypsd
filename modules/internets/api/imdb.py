import urllib
from feed import get_json

class Imdb(object):
	def __init__(self):
		pass #TODO: cache?
	
	def get(self, title):
		url = 'http://www.imdbapi.com/?i=&'
		url += urllib.urlencode({'t': title})
		return get_json(url)
