#!/usr/bin/python pseudoserver.py
# psm_dynloglevel.py
# module for pypsd
# written by Nol888 <nol888@rizon.net>
from psmodule import *

class PSModule_dynloglevel(PSModule):
	"""Allows the logging level to be changed on-the-fly."""

	VERSION = 1
	
	levels = (
		'DEBUG',
		'INFO',
		'WARNING',
		'ERROR',
		'CRITICAL'
	)
		
# Commands
	def cmd_dllLOGLEVEL(self, source, target, pieces):
		if not pieces: return False
		
		rootLogger = logging.getLogger('')
		loglevel = getattr(logging, pieces[0].upper())
		
		rootLogger.setLevel(loglevel)
		self.parent.privMsg(target, "Set logging level to %s" % pieces[0].upper())
		return True
	
	def cmd_dllLOGLEVELS(self, source, target, pieces):
		self.parent.privMsg(target, "Available loglevels: " + ", ".join(self.levels))
		return True
	
	def getCommands(self):
		return (
			('loglevel', {
			'permission':'r',
			'callback':self.cmd_dllLOGLEVEL,
			'usage':"<new level> - Changes the pypsd logfile logging level to the specified level."}),
			('loglevels', {
			'permission':'',
			'callback':self.cmd_dllLOGLEVELS,
			'usage':"- Displays a list of log level flags."}),
		)
