import json
import socket
import urllib2
import xpath
from BaseHTTPServer import BaseHTTPRequestHandler
from decimal import Decimal
from StringIO import StringIO
from urlparse import urlparse
from xml.dom.minidom import Element
from xml.dom.minidom import parse

class InputError(Exception):
	def __init__(self, msg):
		self.msg = msg

	def __str__(self):
		return str(self.msg)

class FeedError(Exception):
	def __init__(self, e):
		if hasattr(e, 'code'):
			c = e.code

			if c == 404:
				self.msg = 'not found.'
			elif c == 406:
				self.msg = 'this eRepublik API feed is unavailable.'
			elif c == 500:
				self.msg = 'eRepublik server has encountered an unexpected error.'
			elif c == 502:
				self.msg = 'invalid response from eRepublik server. Try again later.'
			elif c == 503:
				self.msg = 'eRepublik API feed is temporarily unavailable. Try again later.'
			else:
				self.msg = 'something went wrong while connecting to eRepublik API feed (%s)' % BaseHTTPRequestHandler.responses[e.code][0]

			self.code = c
			self.url = e.url
		elif hasattr(e, 'reason'):
			r = str(e.reason)

			if r == 'timed out':
				self.msg = 'connection to eRepublik API feed timed out. Try again later.'
			else:
				self.msg = r

			self.code = None
			self.url = None
		else:
			e = unicode(e)
			
			if e == 'an error occured':
				self.msg = 'this command cannot work because eRepublik admins disabled the API. If you want to complain, send a ticket: http://www.erepublik.com/en/tickets'
			else:
				self.msg = e
			
			self.code = None
			self.url = None
	
	def __str__(self):
		return self.msg

class HtmlFeed:
	def __init__(self, value):
		if value == None:
			raise InputError('Invalid feed input.')

		if isinstance(value, str) or isinstance(value, unicode):
			try:
				opener = urllib2.build_opener()
				opener.addheaders = [('User-Agent', 'Rizon eRepublik bot - www.rizon.net')]
				feed = opener.open(value.replace(' ', '%20'), timeout=20)
				self._html = feed.read()
				feed.close()
			except urllib2.URLError, e:
				raise FeedError(e)
		else:
			raise InputError('Invalid feed input type.')

	def html(self):
		return self._html

def get_json(value):
	if value == None:
		raise InputError('Invalid feed input.')
	
	if isinstance(value, basestring):
		feed = HtmlFeed(value)
		return json.load(StringIO(feed.html()))
	else:
		raise InputError('Invalid feed input type.')

class XmlFeed:
	def __init__(self, value, namespaces = None):
		if value == None:
			raise InputError('Invalid feed input.')

		self.namespaces = {} if namespaces == None else namespaces

		if isinstance(value, basestring):
			feed = HtmlFeed(value)
			self._element = parse(StringIO(feed.html()))
		elif isinstance(value, Element):
			self._element = value
		else:
			raise InputError('Invalid feed input type.')

		error = xpath.findvalue('/error/message', self._element)

		if error != None:
			raise FeedError(error)

	def elements(self, query):
		return [XmlFeed(x, self.namespaces) for x in xpath.find(query, self._element, namespaces=self.namespaces)]

	def text(self, query, default=None):
		result = xpath.findvalue(query, self._element, namespaces=self.namespaces)

		if not result:
			value = default
		else:
			value = result.strip()

		if isinstance(value, unicode):
			try:
				value = value.encode('latin-1').decode('utf-8')
			except:
				pass
		
		return value

	def int(self, query, default = None):
		result = self.text(query, None)

		if result == None:
			return default

		try:
			return int(result)
		except:
			return default

	def decimal(self, query, default = None):
		result = self.text(query, None)

		if result == None:
			return default

		try:
			return Decimal(result)
		except:
			return default

	def bool(self, query, default = None):
		result = self.text(query, None)

		if result == None:
			return default

		if 'true' in result.lower() or result == '1':
			return True
		elif 'false' in result.lower() or result == '0':
			return False
		else:
			try:
				return int(result) > 0
			except:
				return default
