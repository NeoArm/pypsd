def db(module):
	db_version = module.options.get('db_version', int, 1)

	while db_version in __upgrades:
		__upgrades[db_version](module)
		module.elog.operation('Upgraded db v%d to db v%s.' % (db_version, db_version + 1))
		db_version += 1
		module.options.set('db_version', db_version)

def __upgrade_1(module):
	try:
		module.dbp.execute("CREATE TABLE IF NOT EXISTS erepublik_users (id INT NOT NULL AUTO_INCREMENT, name VARCHAR(51) NOT NULL, flags BIGINT NOT NULL, ban_source TEXT, ban_reason TEXT, citizen INT, company INT, PRIMARY KEY (id), UNIQUE KEY (name))")
		module.dbp.execute("SELECT * FROM erepublik_nicks")
		old_users = module.dbp.fetchall()
		module.dbp.executemany("REPLACE INTO erepublik_users (name, flags, ban_source, ban_reason, citizen, company) VALUES (%s, %s, NULL, NULL, %s, NULL)", [(user[0], 1, user[1]) for user in old_users])
	except Exception, err:
		module.elog.error('User migration failed: @b%s@b' % err)

	try:
		module.dbp.execute("SELECT * FROM erepublik")
		old_channels = module.dbp.fetchall()
		module.dbp.execute("CREATE TABLE IF NOT EXISTS erepublik_channels (id INT NOT NULL AUTO_INCREMENT, name VARCHAR(51) NOT NULL, flags BIGINT NOT NULL, ban_source TEXT, ban_reason TEXT, PRIMARY KEY (id), UNIQUE KEY (name))")
		module.dbp.executemany("REPLACE INTO erepublik_channels (name, flags, ban_source, ban_reason) VALUES (%s, %s, NULL, NULL)", [(channel[1], 1) for channel in old_channels])
	except Exception, err:
		module.elog.error('Channel migration failed: @b%s@b' % err)

def __upgrade_2(module):
	try:
		module.dbp.execute("ALTER TABLE erepublik_users ADD ban_date INT AFTER ban_reason;")
		module.dbp.execute("ALTER TABLE erepublik_users ADD ban_expiry INT AFTER ban_date;")
		module.dbp.execute("ALTER TABLE erepublik_channels ADD ban_date INT AFTER ban_reason;")
		module.dbp.execute("ALTER TABLE erepublik_channels ADD ban_expiry INT AFTER ban_date;")
	except Exception, err:
		module.elog.error('Ban system modification failed: @b%s@b' % err)

def __upgrade_3(module):
	try:
		module.dbp.execute("ALTER TABLE erepublik_options MODIFY value TEXT;")
	except Exception, err:
		module.elog.error('Option system migration (VARCHAR(51) -> TEXT) failed: @b%s@b' % err)

def __upgrade_4(module):
	try: # move MASK_NEWS to 3
		module.dbp.execute("UPDATE erepublik_channels SET flags = ((flags & ~(1 << 2)) | (((flags >> 2) & 1) << 3))")
	except Exception, err:
		module.elog.error('Flags migration failed: @b%s@b' % err)

def __upgrade_5(module):
	try: # enable mass flag for everyone
		module.dbp.execute("UPDATE erepublik_users SET flags = flags | (1 << 2)")
	except Exception, err:
		module.elog.error('Flags migration failed: @b%s@b' % err)

__upgrades = {
	1: __upgrade_1,
	2: __upgrade_2,
	3: __upgrade_3,
	4: __upgrade_4,
	5: __upgrade_5
}
