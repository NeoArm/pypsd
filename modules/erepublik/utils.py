import time
from datetime import datetime

class Object:
	def __init__(self, **entries): 
		self.__dict__.update(entries)
	
	def __repr__(self): 
		return '<%s>' % str('\n '.join('%s : %s' % (k, repr(v)) for (k, v) in self.__dict__.iteritems()))

def create_mask(shift, bits):
	return (shift, bits)

def mask(flags, type, value):
	shift = type[0]
	bits = 2 ** (type[1] - 1)

	return (flags & ~(bits << shift)) | (value << shift)

def unmask(flags, type):
	shift = type[0]
	bits = 2 ** (type[1] - 1)

	return (flags >> shift) & bits

def unix_time(datetime):
	return int(time.mktime(datetime.timetuple()))

def parse_timespan(text):
	buffer = ''
	duration = 0

	for char in text:
		if char == 'd':
			duration += int(buffer) * 24 * 60 * 60
			buffer = ''
		elif char == 'h':
			duration += int(buffer) * 60 * 60
			buffer = ''
		elif char == 'm':
			duration += int(buffer) * 60
			buffer = ''
		elif char == 's':
			duration += int(buffer)
			buffer = ''
		else:
			buffer += char

	return duration

def get_timespan(date):
	td = datetime.now() - date
	if td.days > 365:
		y = int(td.days/365)
		return '%d year%s' % (y, 's' if y > 1 else '')
	elif td.days > 0:
		d = td.days
		return '%d day%s' % (d, 's' if d > 1 else '')
	elif td.seconds > 3600:
		h = int(td.seconds/3600)
		return '%d hour%s' % (h, 's' if h > 1 else '')
	elif td.seconds > 60:
		m = int(td.seconds/60)
		return '%d minute%s' % (m, 's' if m > 1 else '')
	else:
		s = td.seconds
		return '%d second%s' % (s, 's' if s > 1 else '')
	
def format_name(name):
	if name.endswith('s'):
		return "%s'" % name
	else:
		return "%s's" % name

def format_thousand(number, add_prefix = False):
	if not isinstance(number, int):
		return str(number)

	text = str(abs(number))

	length = len(text)
	count = (length - 1) / 3

	for i in xrange(1, count + 1):
		text = text[:length - (i * 3)] + ',' + text[length - (i * 3):]

	if number < 0:
		text = '-' + text
	elif add_prefix:
		text = '+' + text

	return text

def format_ordinal(n, add_thousands_sep=False):
	if add_thousands_sep:
		nstr = format_thousand(n)
	else:
		nstr = str(n)
	suffix = {1: 'st', 2: 'nd', 3: 'rd'}.get(4 if 10 <= n % 100 < 20 else n % 10, "th")
	return nstr + suffix

def format_citizen_message(citizen, message, sex=None):
	if citizen == None:
		return message
	else:
		if hasattr(citizen, 'sex'):
			sex = citizen.sex.lower() if citizen.sex != None else ''
		else:
			sex = sex.lower() if sex else ''
	
	if (sex == 'x'):
		message = message.replace('@sep', '@b@c14::@o')
	elif (sex == 'f'):
		message = message.replace('@sep', '@b@c13::@o')
	
	return message

def format_ascii_irc(message):
	return message.replace('@errsep', '@b@c4::@o').replace('@nsep', '@b@c7::@o').replace('@sep', '@b@c10::@o').replace('@b', chr(2)).replace('@c', chr(3)).replace('@o', chr(15)).replace('@u', chr(31))

def strip_ascii_irc(message):
	stripped = ''

	for char in message:
		if char not in [chr(2), chr(15), chr(22), chr(31)]: #Not actually stripping color codes, but we don't need that (yet)
			stripped += char

	return stripped
