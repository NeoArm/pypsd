#!/usr/bin/python pseudoserver.py
# psm_moo.py
# module for pypseudoserver
# written by ElChE <elche@rizon.net>, martin <martin@rizon.net>
#
# vHost module

import sys
import traceback
import re
from datetime import datetime
from moo import cmd_user
from moo import sys_log, sys_requests
from moo.utils import format_ascii_irc
from psmodule import *

class PSModule_moo(PSModule):
	VERSION = 1
	VERSION_MINOR = 0
	initialized = False
	# Regexes for HostServ messages because 1.8/1.9 duality.
	req_regexes = {'req18': re.compile('^New vHost (.+?) requested by (.+?) \((.+)\)$'),
			'req19': re.compile('^COMMAND: (.+?)!.+?@.+? used request \((.+?)\) to request new vhost (.+)$'),
			'rejacc18': re.compile('^Host Request for (.+?) (.+?) by (.+)$'),
			'acc19': re.compile('^COMMAND: (.+?)!.+?@.+? used activate for (.+?) for vhost .+$'),
			'rej19': re.compile('^COMMAND: (.+?)!.+?@.+? used reject to reject vhost for (.+?) \(.*\)$'),
			'wait18': re.compile("^#\d+ Nick:\002(.+?)\002, vhost:\002(.+?)\002 \(.+\)$"),
			'wait19': re.compile('^\d+ +([^ ]+) +([^ ]+) +([^ ]+) +.+$')
			}

	
	def __init__(self, parent, config):
		PSModule.__init__(self, parent, config)
		self.log = logging.getLogger(__name__)

		try:
			self.chan = istring(config.get('vhost', 'channel'))
			self.logchan = istring(config.get('vhost', 'logchan'))
		except Exception, err:
			self.log.exception("Error reading 'vhost:channel' configuration option: %s" % err)
			raise
		
	def startup(self):
		if not PSModule.startup(self):
			return False

		try:
			self.uid, user = self.parent.createFakeUser(
				self.config.get('vhost', 'nick'),
				self.config.get('vhost', 'user'),
				self.config.get('vhost', 'host'),
				self.config.get('vhost', 'gecos'),
				self.config.get('vhost', 'modes'),
				self.config.get('vhost', 'nspass'),
				join_chans = [self.chan, self.logchan])
			self.nick = self.config.get('vhost', 'nick')
		except Exception, err:
			self.log.exception("Error creating user for vhost module, is the config file correct? (%s)" % err)
			raise
		
		try:
			self.elog = sys_log.LogManager(self)
			self.commands_user = cmd_user.UserCommandManager()
			self.requests = sys_requests.RequestManager(self)
		except Exception, err:
			self.log.exception('Error initializing core subsystems for vhost module (%s)' % err)
			raise
		
		self.elog.debug('Started core subsystems.')		
		self.initialized = True
		
		self.msg('HostServ', 'WAITING') # Check if we missed requests while down
		
		return True

	def shutdown(self):
		PSModule.shutdown(self)
		
		moo_modules = [module for module in sys.modules if module.startswith('modules.moo')]

		for module in moo_modules:
			sys.modules.pop(module)
			
		self.parent.quitFakeUser(self.uid)
	
	def getVersion(self):
		return self.VERSION
	
	def msg(self, target, message):
		if message != '':
			self.parent.privMsg(target, format_ascii_irc(message), self.uid)
	
	def notice(self, target, message):
		if message != '':
			self.parent.notice(target, format_ascii_irc(message), self.uid)
	
	def execute(self, manager, command, argument, channel, sender):
		full_command = '%s%s' % (command, ' %s' % argument if len(argument) else '')
		cmd = manager.get_command(command)
		
		if not cmd:
			return
		
		try:
			cmd[0](self, manager, channel, sender, argument)
		except Exception, e:
			tb = traceback.extract_tb(sys.exc_info()[2])
			longest = 0

			for entry in tb:
				length = len(entry[2])

				if length > longest:
					longest = length

			self.elog.exception(e)
			self.log.exception("moo error!")

			for entry in tb:
				self.elog.traceback('@b%-*s@b : %d %s' % (longest, entry[2], entry[1], entry[3]))
		

	def moo_NOTICE(self, prefix, params):
		msg = params[1].strip()

		if msg.startswith('[#'): #It's a channel welcome message. Let's ignore it.
			return
		
		uid, user = self.parent.get_user(params[0])
		foo, userinfo = self.parent.get_user(prefix)

		if uid != self.uid:
			return

		if userinfo['nick'] != 'HostServ':
			return
		
		# Check for 1.8 or 1.9 requests.
		# 1.8:waiting: #$num Nick:\002$nick\002, vhost:\002$vhost\002 ($display - $date)
		# 1.9:waiting: $num     $nick   $display  $vhost  $date
		sp = msg.split(' ')
		
		# Regex. Because it solves the problem in the easiest way.
		match = self.req_regexes['wait18'].match(msg)
		if match == None:
			match = self.req_regexes['wait19'].match(msg)
			if match == None or match.groups(0) == 'Number':
				return

		if len(match.groups()) == 3:
			(nick, vhost, display) = match.groups()
		elif len(match.groups()) == 2:
			(nick, vhost) = match.groups()
			display = None # self.requests.add can handle None for display nick
		else:
			return

		if not nick or not vhost:
			return

		try:
			self.requests.add(vhost, nick, display)
		except Exception, e:
			tb = traceback.extract_tb(sys.exc_info()[2])
			longest = 0
	
			for entry in tb:
				length = len(entry[2])
	
				if length > longest:
					longest = length
	
			self.elog.exception(e)
			self.log.exception("moo error!")
	
			for entry in tb:
				self.elog.traceback('@b%-*s@b : %d %s' % (longest, entry[2], entry[1], entry[3]))

	def moo_PRIVMSG(self, prefix, params):
		foo, userinfo = self.parent.get_user(prefix)
		bar, myself = self.parent.get_user(self.uid)

		sender = userinfo['nick']
		channel = params[0]
		msg = params[1].strip()
		index = msg.find(' ')

		if index == -1:
			command = msg
			argument = ''
		else:
			command = msg[:index]
			argument = msg[index + 1:]

		if channel == myself['nick']:
			pass #private message
		elif channel == self.chan and command.startswith('!'):
			self.execute(self.commands_user, command, argument, channel, sender)
		elif channel == self.logchan and (sender == 'Global' or sender == 'HostServ'):
			match = self.req_regexes['req18'].match(msg)
			if match != None:
				try:
					self.requests.add(match.group(1), match.group(2), match.group(3))
				except Exception, e:
					self.print_traceback(e)
				return

			match = self.req_regexes['req19'].match(msg)
			if match != None:
				try:
					self.requests.add(match.group(3), match.group(1), match.group(2))
				except Exception, e:
					self.print_traceback(e)
				return

			match = self.req_regexes['rejacc18'].match(msg)
			if match != None:
				(nick, action, by) = match.groups()
				by = by.split(' ')[0] # In case there's a reject reason
				if by == self.nick:
					return

				try:
					if action == 'rejected':
						self.requests.delete(nick)
						self.msg(self.chan, 'vHost for @b%s@b was manually %s by @b%s@b' % (nick, action, by))
					elif action == 'activated':
						self.requests.approve(nick, by, silent=True)
						self.msg(self.chan, 'vHost for @b%s@b was manually %s by @b%s@b' % (nick, action, by))
				except Exception, e:
					self.print_traceback(e)
				return

			match = self.req_regexes['acc19'].match(msg)
			if match != None:
				(by, nick) = match.groups()
				if by == self.nick:
					return

				try:
					self.requests.delete(nick)
					self.msg(self.chan, 'vHost for @b%s@b was manually activated by @b%s@b' % (nick, by))
				except Exception, e:
					self.print_traceback(e)
				return

			match = self.req_regexes['rej19'].match(msg)
			if match != None:
				(by, nick) = match.groups()
				if by == self.nick:
					return

				try:
					self.requests.delete(nick)
					self.msg(self.chan, 'vHost for @b%s@b was manually rejected by @b%s@b' % (nick, by))
				except Exception, e:
					self.print_traceback(e)
				return

	def print_traceback(self, e):
		tb = traceback.extract_tb(sys.exc_info()[2])
		longest = 0

		for entry in tb:
			length = len(entry[2])

			if length > longest:
				longest = length

		self.elog.exception(e)
		self.log.exception("moo error!")

		for entry in tb:
			self.elog.traceback('@b%-*s@b : %d %s' % (longest, entry[2], entry[1], entry[3]))
		

	def getHooks(self):
		return (('notice', self.moo_NOTICE),
				('privmsg', self.moo_PRIVMSG)
				)
