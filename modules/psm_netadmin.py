#!/usr/bin/python pseudoserver.py
#psm-netadmin.py
# module for pypseudoserver
# written by radicand
# doesn't do much by itself

from psmodule import *

class PSModule_netadmin(PSModule):
	VERSION = 1
	
	def startup(self):
		if not PSModule.startup(self): return False
		self.log = logging.getLogger(__name__)
		
		return True

	def cmd_rawu(self, source, target, pieces):
		"""Injects a raw command into the server stream.
		Faked from pypsd user.
		DO NOT USE UNLESS YOU KNOW EXACTLY WHAT YOU ARE DOING
		"""
		
		if not pieces or len(pieces) < 2: return False
		
		uid, user = self.parent.get_user(pieces[0])

		self.parent.sendMessage(pieces[1], ' '.join(pieces[2:]), prefix=uid)
		return True

	def cmd_raw(self, source, target, pieces):
		"""Injects a raw command into the server stream.
		DO NOT USE UNLESS YOU KNOW EXACTLY WHAT YOU ARE DOING
		"""
		
		if not pieces: return False
		
		self.parent.sendMessage(pieces[0], ' '.join(pieces[1:]), prefix=self.parent.sid)
		return True

	def cmd_cmrank(self, source, target, pieces):
		"""Rank and return the top 5 channels using cmodes"""

		if not pieces: return False

		channel_t = self.parent.channel_t

		data = (None,0)
		for k,v in channel_t.iteritems():
			if len(v['members']) > data[1] and pieces[0] in v['modes']: data = (k,len(v['members']))

		self.parent.privMsg(target, "Channel %s is highest for mode (%s) with %d users" %
			(data[0], pieces[0], data[1]))

		return True
	
	def cmd_clookup(self, source, target, pieces):
		"""INTERNAL COMMAND"""

		if not pieces: return False

		cname, chan = self.parent.get_channel(pieces[0])
		if not chan:
			self.parent.privMsg(target, "no such channel")
			return True

		for uid in chan['members']:
			uid2, user = self.parent.get_user(uid)
			if not user:
				uname = ""
				onchan = False
			else:
				uname = user['nick']
				onchan = cname in user['channels']

			self.log.info("CLOOKUPU: %s(%s) %s" %
				(uname, uid, onchan))

		self.parent.privMsg(target, "%s(%d): Modes: %s, Key: %s, Limit: %d; userlist saved to log" %
			(cname, len(chan['members']), chan['modes'], chan['key'], chan['limit']))

		return True

	def cmd_qmsite(self, source, target, pieces):
		"""CTCP WEBSITE ALL qchat / mibbit users"""

		for k,v in self.parent.user_t.iteritems():
			if self.parent.has_umode(k, "W"):
				self.parent.privMsg(k, "\001WEBSITE\001")
		self.parent.privMsg(target, "Done")
		return True

	def getCommands(self):
		return (
			('raw', {
			'permission':'N',
			'callback':self.cmd_raw,
			'usage':"<raw irc message> - sends a raw message (DO NOT ABUSE - FOR TESTING ONLY)"}),
			('rawu', {
			'permission':'N',
			'callback':self.cmd_rawu,
			'usage':"<from> <raw irc message> - sends a raw message (faked from user) (DO NOT ABUSE - FOR TESTING ONLY)"}),
			('cmrank', {
			'permission':'N',
			'callback':self.cmd_cmrank,
			'usage':"<cmode(s)> - returns top 5 channels using the specified cmode(s)"}),
			('clookup', {
			'permission':'N',
			'callback':self.cmd_clookup,
			'usage':"<channel> - log list of names on channel [DEBUG COMMAND]"}),
			('qmsite', {
			'permission':'N',
			'callback':self.cmd_qmsite,
			'usage':"query all online mibbit/qchat users with a CTCP WEBSITE"}),
		)
